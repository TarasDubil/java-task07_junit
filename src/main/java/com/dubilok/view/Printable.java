package com.dubilok.view;

@FunctionalInterface
public interface Printable {
    void print();
}
