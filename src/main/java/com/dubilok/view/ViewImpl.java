package com.dubilok.view;

import com.dubilok.controller.Controller;
import com.dubilok.controller.ControllerImpl;
import com.dubilok.util.UtilMenu;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ViewImpl implements View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ViewImpl() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Start problem Longest Plateua.");
        menu.put("2", "  2 - Start problem Rumours.");
        menu.put("Q", "  Q - exit.");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        controller.showLongestPlateua();
    }

    private void pressButton2() {
        controller.showRumours();
    }

    @Override
    public void show() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
