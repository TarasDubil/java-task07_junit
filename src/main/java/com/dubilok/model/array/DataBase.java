package com.dubilok.model.array;

public interface DataBase {
    int getFirstElement();

    int[] getAllElements();
}
