package com.dubilok.model.array;

import java.util.Arrays;

public class Array {
    private int[] array;

    public Array() {
    }

    public Array(int[] array) {
        this.array = array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int[] getArray() {
        return array;
    }

    @Override
    public String toString() {
        return "LongestPlateua{" +
                "array=" + Arrays.toString(array) +
                '}';
    }
}
