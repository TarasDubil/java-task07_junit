package com.dubilok.model.array;

public class ArrayLogic {

    public static int getSumAllElementsArray(Array array) {
        int sum = 0;
        for (int i = 0; i < array.getArray().length; i++) {
            sum += array.getArray()[i];
        }
        return sum;
    }

    public static int geMaxElementArray(Array array) {
        if (array.getArray() == null) {
            throw new IllegalArgumentException();
        }
        int max = array.getArray()[0];
        for (int i = 1; i < array.getArray().length; i++) {
            if (array.getArray()[i] > max) {
                max = array.getArray()[i];
            }
        }
        return max;
    }

    public static void sortArray(Array array) {
        int arr[] = array.getArray();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length-1; j++) {
                if (arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
        array.setArray(arr);
    }

    public static int[] getElementsFromDataBaseAndSaveToArray(Array array, DataBase dataBase) {
        int[] arr = dataBase.getAllElements();
        array.setArray(arr);
        return arr;
    }

    public static int getFirstElementsFromDataBas(DataBase dataBase) {
        int firstElement = dataBase.getFirstElement();
        return firstElement;
    }

    public static boolean isValueMoreThanZero(int number) {
        return number > 0;
    }

    public static int getMax(int[] array) {
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
    }
}
