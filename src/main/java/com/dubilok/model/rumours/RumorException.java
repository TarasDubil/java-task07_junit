package com.dubilok.model.rumours;

/**
 * Class RumorException uses for throwing an exception when people at the party are less than two.
 */
public class RumorException extends Exception {

    public RumorException() {
        super("People at the party must be more than 2!");
    }
}
