package com.dubilok.model.rumours;

import java.util.Scanner;

/**
 * The class is to estimate the probability that everyone at the party will hear the rumor before it stops propagating.
 * Also, for calculating an estimate of the expected number of people to hear the rumor.
 */
public class Rumors {

    /**
     * hardcoded amount of times we will check to empirically calculate probabilities
     */
    public static final int ATTEMPTS = 100;

    /**
     * The field is meaning how much people should be at the party.
     */
    private int peopleOnTheParty;

    /**
     * The field is meaning how many times all people heard the rumor
     */
    private int countTimesFullSpreaded;

    /**
     * The field is meaning how many people in all attempt heard the rumor
     */
    private int peopleReached;

    /**
     * The constructor initializes field peopleOnTheParty.
     *
     * @param peopleOnTheParty initializes field peopleOnTheParty
     * @throws RumorException throws Exception when people are less than two.
     */
    public Rumors(int peopleOnTheParty) throws RumorException {
        if (peopleOnTheParty <= 2) {
            throw new RumorException();
        }
        this.peopleOnTheParty = peopleOnTheParty;
    }

    public void setPeopleOnTheParty(int peopleOnTheParty) {
        this.peopleOnTheParty = peopleOnTheParty;
    }

    /**
     * Use for reading data
     */
    public static final Scanner SCANNER = new Scanner(System.in);

    /**
     * The method is for counting the average amount of the people that rumor reached.
     *
     * @return The average amount of the people that rumor reached is.
     */
    public int averageAmountOfPeopleThatRumorReached() {
        return peopleReached / ATTEMPTS;
    }

    /**
     * The method is for counting Empirical probability that everyone will hear rumor except Alice in N attempts.
     *
     * @return Empirical probability that everyone will hear rumor in N attempts.
     */
    public double probabilityThatEveryoneWillHearRumor() {
        return (double) countTimesFullSpreaded / ATTEMPTS;
    }

    /**
     * The method is counting amount of people who reached the rumor.
     *
     * @param arr define people reached the rumor or no.
     * @return count people who reached rumor
     */
    private int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    /**
     * The method return true if all people hear rumor else false
     *
     * @param arr contains value if People heard than true else false.
     * @return true if all people heard rumor else false
     */
    private boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }

    /**
     * The method is closing Scanner
     *
     * @param scanner
     */
    private void closeScanner(Scanner scanner) {
        scanner.close();
    }

    /**
     * The method is counting how many people in all attempt heard the rumor.
     * Also how many times all people heard the rumor.
     */
    public void countTimesFullSpreadedAndPeopleReached() {
        for (int i = 0; i < ATTEMPTS; i++) {
            boolean guests[] = new boolean[peopleOnTheParty]; // Alice is zero element, Bob is 1, all other guests - from 2 to N
            guests[1] = true; //Bob already heard
            boolean alreadyHeard = false; //Condition to exit while loop
            int nextPerson = -1; //our random next person
            int currentPerson = 1; //we start from Bob
            while (!alreadyHeard) {
                nextPerson = 1 + (int) (Math.random() * (peopleOnTheParty - 1)); //randomize next person
                if (nextPerson == currentPerson) { // check that it's not our current person
                    while (nextPerson == currentPerson) //if it's true then we just randoming till we get other person
                        nextPerson = 1 + (int) (Math.random() * (peopleOnTheParty - 1));
                }
                if (guests[nextPerson]) //if guest already heard
                {
                    if (rumorSpreaded(guests)) //if all people heard
                        countTimesFullSpreaded++;
                    peopleReached = peopleReached + countPeopleReached(guests); //how many people we get at all
                    alreadyHeard = true; // our condition is true
                }
                guests[nextPerson] = true; //now nextperson heard rumor
                currentPerson = nextPerson; //current person now is nextperson
            }
        }
    }


}
