package com.dubilok.model.longest_plateau;

public class LongestPlateua {

    private int[] arrays;
    private int length = 0;
    private int from = 0;

    public LongestPlateua() {
    }

    public LongestPlateua(int[] arrays) {
        this.arrays = arrays;
    }

    public int[] getArrays() {
        return arrays;
    }

    public int getLength() {
        return length;
    }

    public int getFrom() {
        return from;
    }

    public void setArrays(int[] arrays) {
        this.arrays = arrays;
    }

    public void getLongestPlateua(int[] values) {
        int biggestStartIndex = -1;
        int biggestLength = 0;
        int currentIndex = 1;
        int currentPlateauStartIndex = 1;
        int currentLength = 1;
        boolean plateauStarted = false;
        while (currentIndex < values.length) {
            if (isStartOfPlateau(currentIndex, values)) {
                plateauStarted = true;
                currentPlateauStartIndex = currentIndex;
            } else if (isEndOfPlateau(currentIndex, values)) {
                if ((plateauStarted) && (currentLength > biggestLength)) {
                    biggestLength = currentLength;
                    biggestStartIndex = currentPlateauStartIndex;
                }
                plateauStarted = false;
                currentLength = 1;
            } else {
                currentLength++;
            }
            currentIndex++;
        }
        if (biggestStartIndex < 0) {
            length = -1;
            from = -1;
        } else {
            from = biggestStartIndex;
            length = biggestLength;
        }
    }

    private static boolean isStartOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] < values[index];
    }

    private static boolean isEndOfPlateau(int index, int[] values) {
        if (index <= 0) {
            return false;
        }
        return values[index - 1] > values[index];
    }
}
