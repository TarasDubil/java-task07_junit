package com.dubilok.service;

import com.dubilok.model.longest_plateau.LongestPlateua;
import com.dubilok.model.rumours.RumorException;
import com.dubilok.model.rumours.Rumors;
import com.dubilok.util.UtilMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ManagerImpl implements Manager {

    private Logger logger = LogManager.getLogger(ManagerImpl.class);
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private LongestPlateua longestPlateua;

    @Override
    public void showLongestPlateua() {
        try {
            longestPlateua = new LongestPlateua(UtilMenu.initializeArray(bufferedReader, logger));
        } catch (IOException e) {
            e.printStackTrace();
        }
        longestPlateua.getLongestPlateua(longestPlateua.getArrays());
        logger.info("Length: " + longestPlateua.getLength());
        logger.info("Index from: " + longestPlateua.getFrom());
    }

    @Override
    public void showRumours() {
        try {
            logger.info("Enter how much people should be at the party: ");
            Rumors rumors = new Rumors(Integer.parseInt(bufferedReader.readLine()));
            rumors.countTimesFullSpreadedAndPeopleReached();
            logger.info("Empirical probability that everyone will hear rumor except Alice in "
                    + Rumors.ATTEMPTS + " ATTEMPTS: "
                    + rumors.probabilityThatEveryoneWillHearRumor());
            logger.info("Average amount of people that rumor reached is: "
                    + rumors.averageAmountOfPeopleThatRumorReached());
        } catch (RumorException | IOException e) {
            logger.error(e);
        }
    }
}
