package com.dubilok.controller;

import com.dubilok.service.Manager;
import com.dubilok.service.ManagerImpl;

public class ControllerImpl implements Controller {

    private Manager manager;

    public ControllerImpl() {
        this.manager = new ManagerImpl();
    }

    @Override
    public void showLongestPlateua() {
        manager.showLongestPlateua();
    }

    @Override
    public void showRumours() {
        manager.showRumours();
    }
}
