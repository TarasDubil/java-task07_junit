package com.dubilok.model.array;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ArrayTest {

    DataBase dataBase = mock(DataBase.class);

    Array array = new Array(new int[]{6, 2, 7, 8});

    @Test
    public void getSumAllElementsArrayTest() {
        assertEquals(23, ArrayLogic.getSumAllElementsArray(array));
    }

    @Test
    public void geMaxElementArray() {
        assertTrue(8 == ArrayLogic.geMaxElementArray(array));
    }

    @Test
    public void sortArray() {
        ArrayLogic.sortArray(array);
        assertArrayEquals(new int[]{2, 6, 7, 8}, array.getArray());
    }

    @RepeatedTest(3)
    @Test
    public void getElementsFromDataBase() {
        when(ArrayLogic.getElementsFromDataBaseAndSaveToArray(array, dataBase)).thenReturn(new int[]{1, 2, 3});
        assertArrayEquals(new int[]{1, 2, 3}, dataBase.getAllElements());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 4})
    public void isOdd(int number) {
        assertTrue(ArrayLogic.isValueMoreThanZero(number));
    }

    @Test
    public void getFirstElementFromDataBase() {
        when(ArrayLogic.getFirstElementsFromDataBas(dataBase)).thenReturn(1);
        assertEquals(dataBase.getFirstElement(), 1);
        verify(dataBase).getFirstElement();
    }

    @ParameterizedTest
    @MethodSource("createArguments")
    public void maxArray(int[] array, int max) {
        int maxArray = ArrayLogic.getMax(array);
        assertEquals(max, maxArray);
    }

    private static Stream<Arguments> createArguments() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 4}, 4),
                Arguments.of(new int[]{2, 4, 6, 8}, 8),
                Arguments.of(new int[]{2, 4, 6, 99}, 99));
    }
}
