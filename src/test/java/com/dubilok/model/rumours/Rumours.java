package com.dubilok.model.rumours;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class Rumours {

    private Rumors rumors;

    {
        try {
            rumors = new Rumors(3);
        } catch (RumorException e) {
            e.printStackTrace();
        }
    }

    @ParameterizedTest
    @MethodSource("createArgumentsProbability")
    public void getProbability(int count, double probability) {
        rumors.setPeopleOnTheParty(count);
        rumors.countTimesFullSpreadedAndPeopleReached();
        double probabilityThatEveryoneWillHearRumor = rumors.probabilityThatEveryoneWillHearRumor();
        assertEquals(probability, probabilityThatEveryoneWillHearRumor, 0.1);
    }

    private static Stream<Arguments> createArgumentsProbability() {
        return Stream.of(
                Arguments.of(3, 1.0),
                Arguments.of(4, 0.45),
                Arguments.of(10, 0.0),
                Arguments.of(34, 0.0),
                Arguments.of(50, 0.0)
        );
    }
}
