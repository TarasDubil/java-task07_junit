package com.dubilok.model.longest_plateau;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class LongestTest {

    private LongestPlateua longestPlateua = new LongestPlateua(new int[]{1, 2, 3, 5, 5, 5, 4, 4, 3, 2, 1});

    @ParameterizedTest
    @MethodSource("createArgumentsLength")
    public void getLength(int[] array, int length) {
        longestPlateua.setArrays(array);
        longestPlateua.getLongestPlateua(longestPlateua.getArrays());
        assertEquals(longestPlateua.getLength(), length);
    }

    @ParameterizedTest
    @MethodSource("createArgumentsLength")
    public void getFromPosition(int[] array, int from) {
        longestPlateua.setArrays(array);
        longestPlateua.getLongestPlateua(longestPlateua.getArrays());
        assertEquals(longestPlateua.getFrom(), from);
    }

    private static Stream<Arguments> createArgumentsLength() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 5, 5, 5, 4, 4, 3, 2, 1}, 3), //3
                Arguments.of(new int[]{4, 4}, -1),
                Arguments.of(new int[]{4, 2}, -1),
                Arguments.of(new int[]{4, 4}, -1),
                Arguments.of(new int[]{1, 0, 0, 0, 1}, -1));
    }

    private static Stream<Arguments> createArgumentsFrom() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3, 5, 5, 5, 4, 4, 3, 2, 1}, 3),
                Arguments.of(new int[]{4, 4}, -1),
                Arguments.of(new int[]{4, 2}, -1),
                Arguments.of(new int[]{4, 4}, -1),
                Arguments.of(new int[]{1, 0, 0, 0, 1}, -1));
    }
}
